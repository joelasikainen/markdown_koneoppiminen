# Installing markdown

<p> First we need to install markdown extension from the extensions tab in VS code:
![installation](/images/markdowninstall.png)

After installation is **done** we can try _different tricks_ to highlight our thoughts

### This week's daily high temperatures

|    Day   | Temperature |
|----------|-------------|
| Thursday | 9 degrees   |
| Friday   | 7 degrees   |
| Saturday | 9 degrees   |
| Sunday   | 8 degrees   |

```python
    print("This is a python code block")
    return None
```

## To find the gitlab directory for this markdown project:
Follow this link to the directory(https://gitlab.com/joelasikainen/markdown_koneoppiminen)

> We are coming close to the end of the document, according to the author.

1. First item
2. Second item
3. Third item
4. Fourth item

- Assignment was late because I expected the due time to be in line with all the other courses so far
- Other courses have used midnight as the due time
